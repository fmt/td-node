'use strict';

var express = require('express');
var kraken = require('kraken-js');


var options, app;

/*
 * Create and configure application. Also exports application instance for use by tests.
 * See https://github.com/krakenjs/kraken-js#options for additional configuration options.
 */
options = {
    onconfig: function (config, next) {
        /*
         * Add any additional config setup or overrides here. `config` is an initialized
         * `confit` (https://github.com/krakenjs/confit/) configuration object.
         */
        next(null, config);
    }
};

app = module.exports = express();
app.use(
    kraken(options),
    function(req, res, next) {
        var country,
            countryLocales = {
                'PT': {
                    language: 'pt',
                    country: 'PT'
                },
                'US': {
                    language: 'en',
                    country: 'US'
                }
            };

        country = process.env.PROJECT_COUNTRY;
        if(!country) {
            country = 'PT';
        }
        country = country.toUpperCase();
        res.locals.locale = countryLocales[country];
        res.locals.countryCode = res.locals.locale.language + '-' + res.locals.locale.country;

        next();
    }
);
app.on('start', function () {
    console.log('Application ready to serve requests.');
    console.log('Environment: %s', app.kraken.get('env:env'));
    console.log('Country: %s', process.env.PROJECT_COUNTRY);
});
