'use strict';

module.exports = function phoneNumModel() {
    var regexp = /^(\+|00)?[ ]?((\d{3})|(\d{7,12}))$/;
    return {
        regexp: regexp
    };
};
