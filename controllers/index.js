'use strict';

var phoneNumModel = require('../models/index'),
    fs = require('fs');

var validatePhone = function(req, res, next) {
    var fileUpload = req.files.phoneNumber;

    if(fileUpload.name !== '' && fileUpload.size > 0) {
        fs.readFile(fileUpload.path, 'utf8', function(err, data) {
            if(err) {
                res.locals.errors = { error: err };
                next();
            }
            res.locals.fileContent = data;
            next();
        });
    } else {
        res.locals.errors = { error: 'no file' };
        next();
    }
};

var processFile = function(req, res, next) {
    var phoneRules = new phoneNumModel(),
        phoneRegexp = phoneRules.regexp,
        fileContent,
        numArray,
        phoneList = {
            valid: [],
            invalid: []
        };

    fileContent = res.locals.fileContent;
    numArray = fileContent.split(/\r?\n/g).filter(Boolean);
    numArray = numArray.map(function(el) {
        return el.replace(/\s/,'').trim();
    });

    numArray.forEach(function(phone) {
        var phoneTest = phoneRegexp.test(phone);

        if(phoneTest === true) {
            phoneList.valid.push(phone);
        } else {
            phoneList.invalid.push(phone);
        }
    });

    /* TBD: individual
    phoneNumber = req.body.phoneNumber;
    */

    res.locals.model = phoneList;
    next();
};

var sortFile = function(req, res, next) {
    fs.readFile('./public/areacodes', 'utf8', function(err, data) {
        if(err) {
            res.locals.errors = { error: err };
            next();
        }
        var areaCodes = data.split(/\r?\n/g).filter(Boolean),
            validPhones = res.locals.model.valid,
            totals = {},
            grouped = [];

        validPhones.forEach(function(phone) {
            if(phone.length < 6) return;
            var tmp = phone.replace(/^(\+|00)/, '').substr(0,3);

            if(areaCodes.indexOf(tmp) == -1) {
                tmp = tmp.substr(0,2);
            }
            if(areaCodes.indexOf(tmp) == -1) {
                tmp = tmp.substr(0,1);
            }
            if(areaCodes.indexOf(tmp) == -1) {
                return;
            }
            if(totals[tmp] === undefined) {
                totals[tmp] = { cc: tmp, count: 0 };
            }
            var totalTmp = totals[tmp].count;
            totals[tmp].count = totalTmp+1;
        });

        Object.keys(totals).sort().forEach(function(key) {
            grouped.push(totals[key]);
        });

        res.locals.model.grouped = grouped;
        next();
    });
};

module.exports = function(app) {
    app.get('/',
        function (req, res) {
            res.render('index');
        }
    );
    app.post('/',
        validatePhone,
        processFile,
        sortFile,
        function(req, res) {
            res.render('index');
        }
    );
};
