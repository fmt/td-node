#!/bin/bash

export NODE_ENV=development

if [ "$1" == "PT" ] ; then
    export PROJECT_COUNTRY=PT
else
    export PROJECT_COUNTRY=US
fi

if [ "x$2" == "x-a" ] ; then
    npm install
    ./node_modules/bower/bin/bower install
fi
if [ "x$2" == "x-g" -o "x$2" == "x-a" ] ; then
    ./node_modules/grunt-cli/bin/grunt clean build --force
fi

npm start
#nodemon --debug ./server.js
