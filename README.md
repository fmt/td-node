TD Phonesorter
==============

TD Phone number sorter and validator

This is the NodeJS answer to the proposed exercise, part of Talkdesk's recruitment process.

The problem
===========

Given a file, print the count of valid phone numbers by prefix. Only the lines that include a valid phone number (and nothing more) according to the following rules should be accounted for:

 - has either 3 digits or between 7 and 12 digits (inclusive)
 - can have the optional '+' character in the beginning (before any digit)
 - can start with '00', in which case it shouldn't start with the '+' sign
 - if it starts with '00', these two digits don't count to the maximum number of digits
 - cannot have any letters
 - cannot have any symbol aside from the beginning '+' sign
 - cannot have any whitespace between the '+' sign and the first digit but can have any amount of whitespace in all other places

The (expected) output
=====================

For the following input file:

351960000000
00351961111111
351210000000
35112
244910000000

We would expect the result to be (note that the area codes in the output should be alphabetically sorted):

244:1
351:3

NodeJS version, first run
=========================

For the first compile and run, please use start.sh on the td-node subproject folder:

$ cd td-node
$ ./start.sh PT -a


Usage
=====

The NodeJS version is build on KrakenJS and starts a webserver on http://localhost:8000 with a file upload form that takes the input file.
The file is parsed and the answer is given back to the interface.

You can find a sample file on the public folder, under numbers.txt


Acknowledgements
================

A special thank you to Daniela Barros, thanks for the (almost) infinite patience :)


About the author
================

Filipe Miguel Tavares, pls refer to https://www.linkedin.com/in/fmtavares/ for professional info
